AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")

include("shared.lua")

function ENT:Initialize()
	self:SetModel("models/props_lab/reciever01d.mdl")
	self:PhysicsInit(SOLID_VPHYSICS)
	self:SetMoveType(MOVETYPE_VPHYSICS)
	self:SetSolid(SOLID_VPHYSICS)
	self:SetUseType(SIMPLE_USE)
	local phys = self:GetPhysicsObject()
	phys:Wake()
	self.damage = 100
	//self:SetStealerPly(nil)
	//self:SetStealCooldown(0)
	//self:SetStealCooldownStart(0)
	//self:SetIsStolen(false)
end

function ENT:OnTakeDamage(dmg)
	self.damage = self.damage - dmg:GetDamage()
	if (self.damage <= 0) then
		self:Destruct()
		self:Remove()
	end
end

function ENT:Destruct()
	local vPoint = self:GetPos()
	local effectdata = EffectData()
	effectdata:SetStart(vPoint)
	effectdata:SetOrigin(vPoint)
	effectdata:SetScale(0.5)
	util.Effect("Explosion", effectdata)
end

function ENT:Think()
	if self:GetIsStolen() and self:GetStealCooldown() <= 1 then
		local cooldown = self.StealCooldownTime or 300
		local lel = CurTime()+cooldown
		self:SetStealCooldown(lel)
		self:SetStealCooldownStart(CurTime())
	end
end

function ENT:Use(act, ply)
	if self:GetIsStolen() and self:GetStealCooldown() <= CurTime() and ply:IsPlayer() and IsValid(self:GetStealerPly()) and self:GetStealerPly() == ply then
		SafeRemoveEntity(self)
		local price = self.SellPrice or 1000
		ply:addMoney(price)
		DarkRP.notify(ply, 2, 2, "Вы продали ворованную магнитолу и получили "..DarkRP.formatMoney(price))
	end
end

function ENT:Touch(ent)
	if !self.LastTouch then
		self.LastTouch = CurTime()
	end
	if self.LastTouch and self.LastTouch <= CurTime() then
		if IsValid(ent) and ent:IsVehicle() and ent.VehicleTable and ent.VehicleTable.KeyValues and ent.VehicleTable.KeyValues.vehiclescript and ent.VehicleTable.KeyValues.vehiclescript:lower() != "scripts/vehicles/prisoner_pod.txt" and IsValid(ent:getDoorOwner()) then
			local owner = ent:getDoorOwner()
			if !owner:GetNWBool("HasMagnitola", false) and (!self:GetIsStolen() or self:GetStealCooldown() <= CurTime()) then
				owner:SetPData("HasMagnitola", true)
				owner:SetNWBool("HasMagnitola", true)
				local effectdata = EffectData()
				effectdata:SetOrigin(self:GetPos())
				effectdata:SetMagnitude(1)
				effectdata:SetScale(1)
				effectdata:SetRadius(1)
				util.Effect("Sparks", effectdata)
				SafeRemoveEntity(self)
			end
		end
		self.LastTouch = CurTime() + 1
	end
end
