MagnitolaConfig = MagnitolaConfig or {}

MagnitolaConfig.OutOfCarModifier = 0.8

MagnitolaConfig.DefaultPlaylist = {
	[1] = {link = "http://puu.sh/v4udM/d54b6f2dc9.mp3", IsYoutube = false},
	[2] = {link = "http://puu.sh/v4ugh/4f71a4b2b1.mp3", IsYoutube = false},
	[3] = {link = "http://puu.sh/v4uid/27615b850b.mp3", IsYoutube = false},
	[4] = {link = "http://puu.sh/v4ujY/540874d991.mp3", IsYoutube = false},
}

MagnitolaConfig.Radios = {
		[1] = {
			url = "http://podrubasik.ru:8000/live", -- podrubasik
			name = "Podrubas",
		},
		[2] = {
			url = "http://radio02-cn03.akadostream.ru:8108/shanson128.mp3", -- shanson
			name = "Shanson",
		},
		[3] = {
			url = "http://radio.plaza.one/mp3", --vaporvave
			name = "Vaporwave",
		},
		[4] = {
			url = "http://air.radiorecord.ru:8102/pump_320", -- pump
			name = "Pump",
		},
		[5] = {
			url = "http://radio.alania.net:8000/kvk", -- kavkaz 
			name = "Kavkaz",
		},
		[6] = {
			url = "http://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://uk1.internet-radio.com:8118/listen.pls&t=.m3u", -- d and b
			name = "D&B",
		},
}

MagnitolaConfig.HelperRadioTable = {}

for k,v in pairs(MagnitolaConfig.Radios) do
	MagnitolaConfig.HelperRadioTable[v.url] = true
end