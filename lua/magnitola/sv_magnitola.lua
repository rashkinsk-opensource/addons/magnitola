local function resource_AddDir(dir)
	local files,dirs = file.Find(dir .. "/*","GAME")
	for _, fdir in pairs(dirs) do
		resource_AddDir(dir .. "/" .. fdir)
	end

	for k,v in pairs(files) do
		resource.AddFile(dir .. "/" .. v)
	end
end

local function GetCar(ent)
	if IsValid(ent:GetParent() and ent.fphysSeat) then
		return ent:GetParent()
	else
		return ent
	end
end

resource_AddDir("materials/rashkinsk")
resource_AddDir("sound/rashkinsk")
resource.AddFile("resource/fonts/timenormal.ttf")

hook.Add("lockpickStarted", "RashkinskLockpickShit", function(ply, veh, trace)
	if IsValid(veh) and veh:IsVehicle() and veh.VehicleTable and veh:getDoorOwner() and veh:getDoorOwner():GetNWBool("HasSignalization", false) then
		local shouldsignal = math.random(1, 100)
		if shouldsignal <= 95 then
			veh:SetNWBool("WorkingRashSignalization", true)
			DarkRP.notify(veh:getDoorOwner(), 2, 4, "[АВТОЛОКАТОР] На вашем авто сработала сигнализация!")
		else
			DarkRP.notify(ply, 2, 4, "Вы сломали сигнализацию на этом авто.")
			local owner = veh:getDoorOwner()
			owner:SetPData("HasSignalization", false)
			owner:SetNWBool("HasSignalization", false)
			timer.Simple(60, function()
				if IsValid(owner) and owner:IsPlayer() then
					DarkRP.notify(owner, 2, 6, "[АВТОЛОКАТОР] Сигнализация на вашем авто вышла из строя.")
				end
			end)
		end
	end
end)
local sas2 = Sound("rashkinsk/magnitola/signalizationdisable.wav")
hook.Add("onKeysLocked", "RashkinskStopSignalization", function(ent)
	if IsValid(ent) and ent:IsVehicle() then
		if ent:GetNWBool("WorkingRashSignalization", false) then
			ent:SetNWBool("WorkingRashSignalization", false)
		elseif IsValid(ent:getDoorOwner()) and ent:getDoorOwner():GetNWBool("HasSignalization", false) then
			ent:EmitSound(sas2)
		end
	end
end)

hook.Add("onKeysUnlocked", "RashkinskStopSignalization", function(ent)
	if IsValid(ent) and ent:IsVehicle() then
		if ent:GetNWBool("WorkingRashSignalization", false) then
			ent:SetNWBool("WorkingRashSignalization", false)
		elseif IsValid(ent:getDoorOwner()) and ent:getDoorOwner():GetNWBool("HasSignalization", false) then
			ent:EmitSound(sas2)
		end
	end
end)

hook.Add("PlayerEnteredVehicle", "RashkinskSignalization", function(ply, veh)
	if IsValid(veh) and veh:IsVehicle() and IsValid(veh:getDoorOwner()) and veh:getDoorOwner() == ply and veh:GetNWBool("WorkingRashSignalization", false) then
		veh:SetNWBool("WorkingRashSignalization", false)
	end
end)

local lastthink = 0
local sas = Sound("rashkinsk/magnitola/carsignal.wav")

hook.Add("Think", "RashkinskSignalThink", function()
	if !lastthink or lastthink <= CurTime() then
		lastthink = CurTime() + 1

		for k,v in pairs(ents.GetAll()) do
			if IsValid(v) and v:IsVehicle() then
				if v:GetNWBool("WorkingRashSignalization", false) then
					if !v.SignalSound or (v.SignalSoundTime and v.SignalSoundTime <= CurTime()) then
						if !v.SignalSound then
							v.SignalSound = CreateSound(v, sas)
						else
							v.SignalSound:Stop()
						end

						v.SignalSound:Play()
						v.SignalSoundTime = CurTime() + 24
					end

					if v.SignalSound and !v.SignalSound:IsPlaying() then
						v.SignalSound:Play()
					end
					if !v.HazardLightsRash then
						v.HazardLightsRash = true
						if VCMod then
							v:VC_SetHazardLights(true)
						end
					end
					if tostring(v.RunLightsBool) == "nil" then
						v.RunLightsBool = true
					end
					if VCMod then
						v:VC_SetRunningLights(v.RunLightsBool)
					end
					if v.RunLightsBool then
						v.RunLightsBool = false
					else
						v.RunLightsBool = true
					end
					if !v.SignalTimer then
						v.SignalTimer = CurTime() + 150
					end
					if v.SignalTimer and v.SignalTimer <= CurTime() then
						v.SignalTimer = nil
						v:SetNWBool("WorkingRashSignalization", false)
					end
				else
					if v.SignalSound then
						v.SignalSound:Stop()
						v.SignalSound = nil
						v:EmitSound(sas2)
					end
					if v.HazardLightsRash then
						v.HazardLightsRash = nil
						if VCMod then
							v:VC_SetHazardLights(false)
							v:VC_SetRunningLights(false)
						end
					end
					if v.RunLightsBool then
						v.RunLightsBool = nil
					end
				end
			end
		end
	end
end)

hook.Add("EntityRemoved", "StopSignalization", function(ent)
	if IsValid(ent) and ent:IsVehicle() and ent.SignalSound then
		ent.SignalSound:Stop()
		ent:EmitSound(sas2)
	end
end)

hook.Add("VC_EngineExploded", "RashStopSignalization", function(ent)
	if IsValid(ent) and ent:IsVehicle() and ent:GetNWBool("WorkingRashSignalization", false) then
		ent:SetNWBool("WorkingRashSignalization",false)
	end
end)

util.AddNetworkString("RashMagnitola_GetLink")
util.AddNetworkString("RashMagnitola_ChangeData")
util.AddNetworkString("RashMagnitola_BroadcastShit")
util.AddNetworkString("RashMagnitola_BroadcastData")

hook.Add("PlayerEnteredVehicle", "OpenMagnitola", function(ply, veh, role)
	local car = GetCar(veh)
	local anotherply = car:getDoorOwner()
	if IsValid(veh:GetParent()) and veh.fphysSeat or car.VehicleTable
	and car.VehicleTable.KeyValues and car.VehicleTable.KeyValues.vehiclescript
	and car.VehicleTable.KeyValues.vehiclescript:lower() != "scripts/vehicles/prisoner_pod.txt"
	and IsValid(anotherply)
	and (anotherply:GetNWBool("HasMagnitola", false)) then
		ply:SendLua("RashkinskOpenMagnitola()")
	end
end)

hook.Add("PlayerLeaveVehicle", "CloseMagnitola", function(ply, veh, role)
	ply:SendLua("RashkinskCloseMagnitola()")
end)

net.Receive("RashMagnitola_GetLink", function(len, ply)
	local link = net.ReadString()
	local car = GetCar(net.ReadEntity())
	local anotherply = car:getDoorOwner()
	if IsValid(car) and car:IsVehicle()
	and IsValid(anotherply)
	and (anotherply:GetNWBool("HasMagnitola", false)) then
		net.Start("RashMagnitola_BroadcastShit")
			net.WriteString(link)
			net.WriteEntity(car)
			net.WriteString(ply:UniqueID())
		net.Broadcast()
	end
end)

net.Receive("RashMagnitola_ChangeData", function(len, ply)
	local whattodo = net.ReadString()
	local car = GetCar(ply:GetVehicle()) //net.ReadEntity
	if IsValid(car) and car:IsVehicle() then
		if whattodo == "Volume" then
			local volume = net.ReadFloat()
			net.Start("RashMagnitola_BroadcastData")
				net.WriteEntity(car)
				net.WriteString("Volume")
				net.WriteFloat(volume)
			net.Broadcast()
		elseif whattodo == "Time" then
			local time = net.ReadInt(32)
			net.Start("RashMagnitola_BroadcastData")
				net.WriteEntity(car)
				net.WriteString("Time")
				net.WriteInt(time, 32)
			net.Broadcast()
		end
	end
end)

hook.Add("PlayerInitialSpawn", "AssignMagnitola", function(ply)
	timer.Simple(2, function()
		if IsValid(ply) and ply:IsPlayer() then -- in case if player magically disconnects
			ply:SetNWBool("HasMagnitola", tobool( ply:GetPData("HasMagnitola", false) ))
			ply:SetNWBool("HasSignalization", tobool( ply:GetPData("HasSignalization", false) ))
		end
	end)
end)

hook.Add("EntityEmitSound", "SetVolumeToLowerMagnitola", function(data)
	local ent = data.Entity
	if IsValid(ent) and ent:IsVehicle() then
		data.Volume = 0.5
		return true
	end
end)